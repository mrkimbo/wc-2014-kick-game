module.exports = function (grunt)
{
  // Project configuration.
  grunt.initConfig(
    {
      pkg: grunt.file.readJSON('package.json'),
      debug: false,

      // Clean deploy folder:
      clean: {
        deploy: {
          files: [
            {
              dot: true,
              src: [
                'deploy/**/*',
                '**/*.min.min.*'
              ]
            }
          ]
        },
        post: {
          files: [
          	{
          	  src: '.tmp'
          	}
          ]
        }
      },

      // Copy images and html to deploy:
      copy: {
        deploy: {
          files: [
            {
              expand: true,
              flatten: true,
              cwd: 'src/',
              dest: 'deploy/',
              src: [
                'img/*.{jpg,jpeg,png,gif,svg,json}', // images & data
                '*.html' // html files
              ]
            }
          ]
        }
      },

	  // Prepare concatenation/minification rules found in HTML files //
      // NOTE: Requires concat, uglify and cssmin tasks //
      useminPrepare: {
        html: 'src/*.html',
        options: {
          dest: 'deploy'
        }
      },
      
      // Update HTML to inject references to created files //
      usemin: {
        html: 'deploy/*.html'
      },

      replace: {
        dist: {
          options: [
            {
              // Strip paths from image urls.
              find: /([\("']+)\W*?img\//gi,
              repl: '$1'
            },
            {
              // Rename console.log statements to just 'log'.
              find: /([^window\.])console\.\w+?\(/gi,
              repl: '$1log('
            },
            {
              // Update date meta tag.
              find: /(<meta.+?"date".+?content=").*?"/i,
              repl: '$1<%= grunt.template.today("yyyy-mm-dd HH:mm:ss") %>"'
            },
            {
              // Remove comments marked for removal (multiline).
              find: /<!-+~[\s\S]+?-->\n/gi,
              repl: ''
            },
            {
              // Remove items marked for deletion (multiline).
              find: /<.*?(?=data-remove)[\s\S]+?<\/\w+>\n?/gm,
              repl: ''
            },
            {
              // Uncomment items marked for inclusion (multiline).
              find: /<!-+\+([\s\S]+?)-->\n/gi,
              repl: '$1\n'
            }
          ],
          src: [
            'deploy/*.min.{css,js}',
            'deploy/*.html'
          ]
        },
        debug: {
          options: [
            {
              // Add debug flag after banner in main.min.js
              find: /^/,
              repl: 'var DEBUG=<%=debug%>;'
            }
          ],
          src: [
            'deploy/main.min.js'
          ]
        }
      },
      
      // create zip containing project source files
      compress: {
  		src: {
    	  options: {
      	    archive: '<%=pkg.name%>.zip'
    	  },
    	  files: [
    	    {
    	      src: [
    	        './src/**/*',
    	        './bower.json',
    	        './package.json',
    	        './gruntfile.js'
    	      ]
    	    }
    	  ]
  	    }
	  }

    });

  // Load plugins for necessary tasks:
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.registerMultiTask('replace', '', function ()
  {
    var s;

    grunt.log.writeln('Applying regex replace to following files:');
    this.filesSrc.forEach(
      function (file)
      {
        grunt.log.writeln('    - '.white + file.cyan);
        s = grunt.file.read(file);
        this.data.options.forEach(
          function (item)
          {
            s = s.replace(new RegExp(item.find), item.repl);
          });
        grunt.file.write(file, s);
      }, this);
  });

  // Create task to allow debug logs in deploy version:
  grunt.registerTask('debug', function ()
  {
    grunt.config.set('debug', 'true');
    grunt.task.run('default');
  });
  
  // Create pseudonyms for compress:
  grunt.registerTask('export', ['compress']);
  grunt.registerTask('zip', ['compress']);

  grunt.registerTask('init',[
    'less',
    'copy:jshint'
  ]);

  /**
   Flow:
   - Wipe previous build,
   - Copy html/images,
   - build css,
   - build app.js (dynamicData and app entry point),
   - build lib.js (AMD modules),
   - concat lib into app and copy to deploy,
   - remove comments, inject debug flag, fix img paths and remove require script tag,
   - clean up temp files.
   */
  grunt.registerTask('default', [
    'clean:deploy',		// clear previous build.
    'copy',             // copy images and html files to deploy.
    'useminPrepare',	// parse compression/concat rules from html.
    'concat',			// USEMIN: execute concat tasks.
    'uglify',			// USEMIN: execute uglify tasks.
    'cssmin',			// USEMIN: execute cssmin tasks.
    'usemin',			// USEMIN: replace file refs in html.
    'replace',          // fix asset paths in html, add debug flag, etc.
    'clean:post'		// clear tmp files.
  ]);
};