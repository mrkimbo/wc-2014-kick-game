/**
 * Created with JetBrains PhpStorm.
 * User: kimh
 * Date: 13/09/13
 * Time: 11:23 AM
 */

(function(){

  'use strict';

  var Utils = {

    getFontStyle: function(size, colour, align) {
      return {
        font: (size || '12') + 'px Open Sans',
        fill: colour || '#FFFFFF',
        align: align || 'left'
      };
    },

    showHandCursor: function() {
      document.getElementsByTagName('canvas')[0].style.cursor = 'pointer';
    },

    hideHandCursor: function() {
      document.getElementsByTagName('canvas')[0].style.cursor = 'default';
    }
  };

  /**
   * Logging helper.
   * @param {...*} args
   */
  window.log = function()
  {
    if(!window.console || !window.console.log || window.DEBUG === false){
      return;
    }
    // ie9 fix:
    if(typeof console.log === 'object'){
      Function.prototype.bind.call(console.log,console)
        .apply(console,arguments);
    } else {
      console.log.apply(console,arguments);
    }
  };

  // drop into window scope:
  window.utils = Utils;

})();
