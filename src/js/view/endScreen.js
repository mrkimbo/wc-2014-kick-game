/**
 * Created with WebStorm.
 * User: kimh
 * Date: 18/06/2014
 * Time: 14:59
 */

/* global Phaser, Config, utils, log */

(function() {

  'use strict';

  var EndScreen = function(game) {
    Phaser.Group.call(this, game);

    this.game = game;
    this.onGameDecision = new Phaser.Signal();
  };

  EndScreen.EVENT = {
    RESET: 'reset',
    EXIT: 'exit'
  };

  var MESSAGES = [
    'CLOSE - HAVE ANOTHER GO!',
    '- GOOD WORK!',
    '- GREAT WORK!',
    '- PERFECTION!'
  ];
  var PROMO = 'In the Semi-Finals, place a correct score bet, and if the\n' +
    'match goes into extra time, WE\'LL REFUND YOUR BET!';

  EndScreen.prototype = Object.create(Phaser.Group.prototype);
  EndScreen.prototype.constructor = EndScreen;

  EndScreen.prototype.init = function(x, y) {
    this.position.setTo(x, y);
    this.render();
    this.addListeners();

    // initially hidden:
    this.visible = false;
  };

  EndScreen.prototype.render = function() {

    this.messageSprite = this.create(0, 0, 'sprites', 'speech.png');
    this.messageSprite.anchor.setTo(0.5);
    this.messageSprite.position.setTo(
      350, this.game.height * 0.5
    );

    this.createMessages();
    this.createPromo();
    this.createButtons();
  };

  EndScreen.prototype.addListeners = function() {
    var btn = this.resetBtn;
    btn.inputEnabled = true;
    btn.events.onInputOver.add(this.onButtonOver, this);
    btn.events.onInputOut.add(this.onButtonOut, this);
    btn.events.onInputDown.add(this.onResetBtnClick, this);

    btn = this.exitBtn;
    btn.inputEnabled = true;
    btn.events.onInputOver.add(this.onButtonOver, this);
    btn.events.onInputOut.add(this.onButtonOut, this);
    btn.events.onInputDown.add(this.onExitBtnClick, this);
  };

  EndScreen.prototype.createMessages = function() {
    // Custom messages:
    var msg;
    this.resultMessages = this.game.add.group(this);
    this.resultMessages.position.setTo(this.messageSprite.x - 92, 60);
    for (var i = 0; i < MESSAGES.length; i++) {
      msg = this.game.add.text(
        0, 0, i + '/' + Config.MAX_ATTEMPTS + ' ' + MESSAGES[i],
        utils.getFontStyle('24', '#FFFFFF'),
        this.resultMessages
      );
      msg.anchor.setTo(0.5, 0);
      msg.visible = false;
    }
  };

  EndScreen.prototype.createPromo = function() {
    var text = this.game.add.text(
      this.resultMessages.x,
      this.resultMessages.y + 40,
      PROMO,
      utils.getFontStyle('15', '#FAD701', 'center')
    );
    text.anchor.setTo(0.5, 0);
    this.add(text);
  };

  EndScreen.prototype.createButtons = function() {
    // Create hit areas for buttons:
    this.buttons = this.game.add.group(this);
    var bmp = this.game.add.bitmapData(125, 38);
    bmp.fill(0, 0, 0, 0.1);
    this.resetBtn = this.buttons.create(195, 170, bmp);
    this.exitBtn = this.buttons.create(495, 170, bmp);
  };


  EndScreen.prototype.show = function(score) {

    // show appropriate result message:
    this.resultMessages.forEachExists(function(item) {
      item.visible = item.parent.getIndex(item) === score;
    });

    this.visible = true;
  };

  EndScreen.prototype.hide = function() {
    this.alpha = 1;
    this.visible = false;
  };


  EndScreen.prototype.onButtonOver = function(target) {
    utils.showHandCursor();
    target.alpha = 0;
  };

  EndScreen.prototype.onButtonOut = function(target) {
    utils.hideHandCursor();
    target.alpha = 1;
  };

  EndScreen.prototype.onResetBtnClick = function() {
    this.onGameDecision.dispatch(EndScreen.EVENT.RESET);
  };

  EndScreen.prototype.onExitBtnClick = function() {
    this.onGameDecision.dispatch(EndScreen.EVENT.EXIT);
  };


  EndScreen.prototype.toString = function() {
    return 'EndScreen';
  };

  window.EndScreen = EndScreen;

})();
