/**
 * Created with WebStorm.
 * User: kimh
 * Date: 18/06/2014
 * Time: 14:59
 */

/* global Phaser, Config, log */

(function() {

  'use strict';

  var LifeMeter = function(game) {
    Phaser.Group.call(this, game);

    this.game = game;
  };
  LifeMeter.prototype = Object.create(Phaser.Group.prototype);
  LifeMeter.prototype.constructor = LifeMeter;

  LifeMeter.prototype.init = function(x, y) {
    this.render();
    this.position.setTo(x, y);
    this.startX = this.position.x;
    this.reset();
  };

  LifeMeter.prototype.render = function() {
    this.bg = this.create(
      0, 0, 'sprites', 'score_bg.png'
    );

    var life, gap = 16, i = Config.MAX_ATTEMPTS;
    this.lives = this.game.add.group(this);
    this.lives.position.setTo(25, 16);
    while (i--) {
      life = this.lives.create(0, 0, 'sprites', 'score_ball.png');
      life.anchor.setTo(0.5, 0.5);
      life.x += (life.width * i) + (gap * i);
    }

    this.results = this.game.add.group(this);
    this.results.position.setTo(this.lives.x, this.lives.y);
  };

  LifeMeter.prototype.hit = function(attempt) {
    this.showResult(attempt, 'score_made.png');
  };

  LifeMeter.prototype.miss = function(attempt) {
    this.showResult(attempt, 'score_miss.png');
  };

  LifeMeter.prototype.showResult = function(attempt, type) {
    var icon = this.results.create(0, 0, 'sprites', type);
    icon.anchor.setTo(0.5, 0.5);
    var target = this.lives.getAt(Config.MAX_ATTEMPTS - attempt);
    icon.position.x = target.position.x;

    // animate in result icon:
    icon.scale.set(5);
    icon.alpha = 0;
    this.game.add.tween(icon)
      .to({alpha: 1}, 300, Phaser.Easing.Quadratic.In, true);
    this.game.add.tween(icon.scale)
      .to({x: 1, y: 1}, 300, Phaser.Easing.Quadratic.In, true);
  };

  LifeMeter.prototype.reset = function() {
    this.results.removeAll(true);
  };

  LifeMeter.prototype.toString = function() {
    return 'LifeMeter';
  };

  window.LifeMeter = LifeMeter;

})();
