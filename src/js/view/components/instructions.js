/**
 * Created with WebStorm.
 * User: kimh
 * Date: 19/06/2014
 * Time: 15:03
 */

/* global Phaser, Config, log, utils */

(function() {

  'use strict';

  var Instructions = function(game) {
    Phaser.Group.call(this, game);

    this.game = game;
    this._currentMessage = -1;
    this.pulseAnim = null;
  };

  Instructions.MESSAGES = {
    HOLD: 0,
    RELEASE: 1
  };

  Instructions.prototype = Object.create(Phaser.Group.prototype);
  Instructions.prototype.constructor = Instructions;

  Instructions.prototype.init = function(x, y) {
    this.position.setTo(x, y);
    this.render();

    this.visible = false;
  };

  Instructions.prototype.render = function() {
    this.hold = this.create(0, 0, 'sprites', 'click_and_hold.png');
    this.hold.anchor.setTo(0.5, 0.5);
    this.hold.visible = false;

    this.release = this.create(0, 0, 'sprites', 'release.png');
    this.release.anchor.setTo(0.5, 0.5);
    this.release.visible = false;
  };

  Instructions.prototype.setCurrentMessage = function(type) {
    if (type === this._currentMessage) return;
    this._currentMessage.visible = false;
    switch(type) {
      case Instructions.MESSAGES.HOLD:
        this._currentMessage = this.hold;
        break;

      case Instructions.MESSAGES.RELEASE:
        this._currentMessage = this.release;
        break;
    }
    this._currentMessage.visible = true;
  };

  Instructions.prototype.show = function() {
    log(this + '::show()');
    this.visible = true;
    this.scale.setTo(0, 0);
    this.game.add.tween(this.scale).to(
      {x: 1, y: 1}, 300, Phaser.Easing.Quadratic.Out, true
    ).onComplete.add(this.startPulse, this);
  };

  Instructions.prototype.hide = function() {
    if(this.pulseAnim){
      this.pulseAnim.stop();
      this.pulseAnim = null;
    }
    this.game.add.tween(this.scale).to(
      {x: 0, y: 0}, 300, Phaser.Easing.Quadratic.Out, true
    ).onComplete.add(
      function() {
        this.visible = false;
      }, this
    );
  };

  Instructions.prototype.startPulse = function() {
    this.pulseAnim = this.game.add.tween(this.scale)
      .to({x: 0.9, y: 0.9}, 300,
        Phaser.Easing.Quadratic.InOut, true,
        0, Number.MAX_VALUE, true);
  };

  Instructions.prototype.toString = function() {
    return 'Instructions';
  };

  window.Instructions = Instructions;

})();