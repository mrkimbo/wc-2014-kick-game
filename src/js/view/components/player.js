/**
 * Created with WebStorm.
 * User: kimh
 * Date: 18/06/2014
 * Time: 14:57
 */

/* global Phaser, Config, log */

(function() {

  'use strict';

  var Player = function(game, x, y) {
    Phaser.Sprite.call(this, game, x, y, 'sprites', 'player_0.png');
    game.world.add(this);

    this.game = game;
    this.init();
  };
  Player.prototype = Object.create(Phaser.Sprite.prototype);
  Player.prototype.constructor = Player;

  Player.prototype.init = function() {

    this.animations.add(
      'idle', ['player_0.png'], 8, false, false
    );
    this.animations.add(
      'kick',
      ['player_0.png', 'player_7.png', 'player_8.png', 'player_9.png'],
      24, false, false);
    this.animations.add(
      'return', [
        'player_9.png', 'player_8.png', 'player_7.png', 'player_0.png'
      ], 24, false, false
    );

    // set initial frame:
    this.animations.play('idle');
  };

  Player.prototype.show = function() {
    this.game.add.tween(this).to(
      {x: 10}, 500, Phaser.Easing.Bounce.Out, true
    );
  };

  Player.prototype.kick = function() {
    this.animations.play('kick');
  };
  Player.prototype.return = function() {
    this.animations.play('return');
  };

  Player.prototype.setPose = function(power) {
    var idx = Math.round(6 * power);
    this.frameName = 'player_' + idx + '.png';
  };

  Player.prototype.toString = function() {
    return 'Player';
  };

  window.Player = Player;

})();
