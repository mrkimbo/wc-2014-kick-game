/**
 * Created with WebStorm.
 * User: kimh
 * Date: 18/06/2014
 * Time: 14:59
 */

/* global Phaser, Config, log, utils */

(function() {

  'use strict';

  var PowerMeter = function(game) {
    Phaser.Group.call(this, game);

    this.game = game;
    this.anim = null;
    this.onStart = new Phaser.Signal();
  };

  var TARGET_POINTS = [100, 165, 230],
    TARGET_NAMES = ['target_low.png', 'target_mid.png', 'target_top.png'];

  PowerMeter.prototype = Object.create(Phaser.Group.prototype);
  PowerMeter.prototype.constructor = PowerMeter;

  PowerMeter.prototype.init = function(x, y) {
    this.position.setTo(x, y);

    this.render();
    this.addListeners();
    this.createAccessors();
    this.setEnabled(false);
    this.reset();
  };

  PowerMeter.prototype.render = function() {

    this.bg = this.create(0, 0, 'sprites', 'power_meter.png');
    this.bar = this.create(12, 4, 'sprites', 'gradient.png');
    this.fill = this.create(12, 4, 'sprites', 'meter_overlay.png');
    this.fill.alpha = 0.75;
    this.fill.mask = this.createFillMask();

    this.button = this.create(0, -15, 'sprites', 'button.png');
    this.button.anchor.setTo(0.5, 0);

    this.width = this.getBounds().width;

    this.createTargetPoints();
    this.createMeterLines();
  };

  PowerMeter.prototype.addListeners = function() {
    this.button.inputEnabled = true;
    this.button.events.onInputOver.add(function() {
      this.game.canvas.style.cursor = 'pointer';
    }, this);
    this.button.events.onInputOut.add(function() {
      this.game.canvas.style.cursor = 'default';
    }, this);
  };

  PowerMeter.prototype.createFillMask = function() {
    var mask = this.game.add.graphics(0, 0);
    mask.beginFill(0xCC0000);
    mask.drawRect(
      this.fill.x,
      this.fill.y,
      this.fill.width,
      this.fill.height
    );
    mask.endFill();
    return this.add(mask);
  };

  PowerMeter.prototype.createTargetPoints = function() {
    // create target lines and labels:
    var target, bmp = this.game.add.bitmapData(2, 11);
    bmp.fill(255, 255, 255, 0.7);
    for (var i = 0; i < TARGET_POINTS.length; i++) {
      this.create(TARGET_POINTS[i] + 1, 6, bmp);
      target = this.create(
          TARGET_POINTS[i] + 2, -5, 'sprites', TARGET_NAMES[i]
      );
      target.anchor.setTo(0.5, 0);
      target.scale.setTo(0.25);
    }
  };

  PowerMeter.prototype.getPointDistance = function(x1, x2) {
    var offset = this.fill.x - 15;
    return Math.abs(x1 - x2 - offset);
  };

  PowerMeter.prototype.getNearestTarget = function(x) {
    for(var i=0; i<TARGET_POINTS.length; i++) {
      if(this.getPointDistance(TARGET_POINTS[i],x) <= 20) return i;
    }
    return -1;
  };

  PowerMeter.prototype.createMeterLines = function() {
    var offset = this.fill.x - 15;
    var getDiff = function(a, b) {
      return Math.abs(a - b - offset);
    };
    var getAlpha = function(x) {
      var min = 0.25, max = 0.9 - min;
      var p = TARGET_POINTS.filter(function(p) {
        return getDiff(p, x) <= 20;
      });
      if (!p.length) return min;
      //log(getDiff(p[0], x));
      if (getDiff(p[0], x) - 2 === 0) return 0;

      var percent = (getDiff(p[0], x) / 25);
      return min + (max - (max * percent));
    };

    var bmp, x = this.fill.x - 1, line,
      max = x + this.fill.width;
    bmp = this.game.add.bitmapData(2, 6);
    bmp.fill(255, 255, 255, 0.7);
    while (x += 5, x < max) {
      line = this.create(x, 9, bmp);
      line.alpha = getAlpha(x);
    }
  };

  PowerMeter.prototype.start = function() {
    // start power bar animation:
    this.value = 0.01;
    this.anim = this.game.add.tween(this)
      .to({value: 1}, Config.POWER_BAR_SPEED,
          Phaser.Easing.Quadratic.InOut,
          true, 0, Number.MAX_VALUE, true
    );
  };

  PowerMeter.prototype.stop = function() {
    if(this.anim) this.anim.stop();
    this.anim = null;

    // find nearest target point:
    //log(this.value, this.getNearestTarget(this.fill.width * this.value));
  };

  PowerMeter.prototype.reset = function() {
    this.game.add.tween(this)
      .to({value: 0.01}, 1000, Phaser.Easing.Quadratic.Out, true, 400);
  };

  PowerMeter.prototype.setEnabled = function(bEnabled) {
    this.button.input.enabled = bEnabled;
  };

  PowerMeter.prototype.createAccessors = function() {
    Object.defineProperty(this, 'value', {
      get: function() {
        return this.fill.mask.scale.x;
      },
      set: function(v) {
        this.fill.mask.scale.x = Phaser.Math.clamp(v, 0, 1);
      }
    });
  };

  PowerMeter.prototype.toString = function() {
    return 'PowerMeter';
  };

  window.PowerMeter = PowerMeter;

})();
