/**
 * Created with WebStorm.
 * User: kimh
 * Date: 12/06/2014
 * Time: 16:03
 */

/* global Config, Phaser, LifeMeter, PowerMeter, Instructions, Player,
   EndScreen, DoubleClick, Enabler, log, utils
*/

(function() {

  'use strict';

  var GameView = function() {};

  GameView.prototype = {

    init: function(game, model) {
      this.game = game;
      this.model = model;
      this.onGameComplete = new Phaser.Signal();
      this.render();

      this.addListeners();
    },

    render: function() {
      log(this + '::render()');

      this.createGround();
      this.createLifeMeter();
      this.createPlayer();
      this.createPowerMeter();
      this.createInstructions();
      this.createTargets();
      this.createHitSprite();
      this.createBall();
      this.createOverlay();
      this.createEndScreen();
      this.createGameElements();
    },

    createGround: function() {
      // Create some ground:
      var bmp = this.game.add.bitmapData(this.game.width, 20);
      bmp.fill(200, 0, 0, 0);
      this.ground = this.game.add.sprite(0, this.game.height - bmp.height, bmp);
      this.game.physics.enable(this.ground, Phaser.Physics.ARCADE);
      this.ground.body.allowGravity = false;
      this.ground.body.immovable = true;

      // Exit CTA Button
      bmp = this.game.add.bitmapData(134, 32);
      bmp.fill(0, 0, 0, 0.1);
      this.exitBtn = this.game.add.sprite(574, 63, bmp);
      this.exitBtn.angle = 9;
      //this.exitBtn.visible = false;
    },

    createLifeMeter: function() {
      this.lifeMeter = new LifeMeter(this.game);
      this.lifeMeter.init(10, 10);
    },

    createPlayer: function() {
      this.player = new Player(this.game, -125, 0);
      this.player.y = this.ground.y - this.player.height + 2;
    },

    createPowerMeter: function() {
      this.powerMeter = new PowerMeter(this.game);
      this.powerMeter.init(0, this.game.height + 15);
      this.powerMeter.x = Math.round(
        (this.game.width - this.powerMeter.width) * 0.5
      ) + 20;
    },

    createInstructions: function() {
      // Instructions:
      this.instructions = new Instructions(this.game);
      this.instructions.init(
          this.powerMeter.x - 5,
          this.game.height - 85
      );
      this.instructions.setCurrentMessage(Instructions.MESSAGES.HOLD);
    },

    createTargets: function() {
      this.targetGroup = this.game.add.group();
      this.targetGroup.enableBody = true;
      this.targetGroup.physicsBodyType = Phaser.Physics.ARCADE;
      var target, names = ['target_low', 'target_mid', 'target_top'];
      while (names.length) {
        target = this.targetGroup.create(
          0, 80 * (names.length - 1), 'sprites',
          names.shift().toString() + '.png'
        );
        target.anchor.setTo(0.5, 0.5);
        // reduce size of physics body to make it harder to hit:
        target.body.setSize(10, target.height * 0.75, 0, 0);
        target.body.allowGravity = false;
        target.body.immovable = true;
        target.scale.x = 0;
      }
      this.targetGroup.position.setTo(this.game.width - 60, 45);
    },

    createBall: function() {
      // Ball trail holder:
      this.trailPool = this.game.add.group();

      // Ball:
      this.ball = this.game.add.sprite(150, -20, 'sprites', 'football.png');
      this.ball.startPos = new Phaser.Point(this.ball.x, this.ball.y);

      this.ball.anchor.setTo(0.5, 0.5);
      this.game.physics.enable(this.ball, Phaser.Physics.ARCADE);
      this.ball.body.setSize(
          this.ball.width * 0.5,
          this.ball.height * 0.5
      );
      this.ball.events.onOutOfBounds.add(this.onBallOutOfBounds, this);
      this.ball.kill();
    },

    createOverlay: function() {
      this.overlay = this.game.add.graphics(0, 0);
      this.overlay.beginFill(0x000000, 0.9);
      this.overlay.drawRect(0, 0, this.game.width, this.game.height);
      this.overlay.alpha = 0;
      this.overlay.visible = false;
    },

    createHitSprite: function() {
      this.hitAnim = this.game.add.group();
      this.hitAnim.stars = this.hitAnim.create(0, -20, 'sprites', 'star1.png');
      this.hitAnim.stars.anchor.setTo(0.5, 0.27);
      this.hitAnim.stars.animations.add(
        'burst', ['star1.png', 'star2.png', 'empty.png'],
        15, false, false
      );
      this.hitAnim.label = this.hitAnim.create(
        0, 0, 'sprites', 'made.png'
      );
      this.hitAnim.label.anchor.setTo(0.5, 0.5);
      this.hitAnim.visible = false;
    },

    createEndScreen: function() {

      this.endScreen = new EndScreen(this.game);
      this.endScreen.init(0, 0);
    },

    createGameElements: function() {

      // Win/Lose Dude:
      this.dude = this.game.add.group();
      this.dude.body = this.dude.create(0, 0, 'sprites', 'dude.png');
      this.dude.body.anchor.setTo(0.5, 0.5);
      this.dude.rotation = Phaser.Math.degToRad(0);
      this.dude.showPos = new Phaser.Point(
          this.game.width - 150, this.game.height - 80
      );
      this.dude.hidePos = new Phaser.Point(
          this.game.width + 50, this.game.height + 50
      );
      this.dude.position.setTo(this.dude.hidePos.x, this.dude.hidePos.y);
      this.dude.visible = false;

      // Miss sign:
      this.sign = this.game.add.sprite(
        this.game.width, this.game.height - 240, 'sprites', 'miss.png'
      );
      this.sign.hidePos = new Phaser.Point(
        this.game.width,
        this.sign.y
      );
      this.sign.showPos = new Phaser.Point(
          this.game.width - 160,
          this.sign.y
      );
      this.sign.visible = false;
    },

    addListeners: function() {
      // wire up power-meter button:
      var btn = this.powerMeter.button;

      this.powerMeter.button.events.onInputDown.add(
        this.onPowerMeterClick, this
      );

      this.endScreen.onGameDecision.add(this.onEndScreenDecision, this);

      btn = this.exitBtn;
      btn.inputEnabled = true;
      btn.events.onInputOver.add(this.onButtonOver, this);
      btn.events.onInputOut.add(this.onButtonOut, this);
      btn.events.onInputDown.add(this.onExitBtnClick, this);
    },


    // --------------------------- GAME MECHANICS --------------------------- //

    playIntro: function() {
      log(this + '::intro()');

      // show player:
      this.player.show();

      // show targets
      this.targetGroup.forEach(function(target) {
        this.game.add.tween(target.scale).to(
          {x: 1}, 600, Phaser.Easing.Bounce.Out,
          true, 500 + (60 * this.targetGroup.getIndex(target))
        );
      }, this);

      // show powerMeter:
      this.game.add.tween(this.powerMeter).to(
        {y: this.game.height - 50}, 500, Phaser.Easing.Bounce.Out, true, 1000
      ).onComplete.add(this.reset, this);

      // show disclaimer:
      document.getElementById('disclaimer').style.opacity = 1;
    },

    onPowerMeterClick: function() {
      switch (this.model.state) {
        case Config.STATE.IDLE:
          this.startPowerMeter();
          break;

        case Config.STATE.POWER:
          this.kick();
          break;

        default:
          return;
      }
    },

    startPowerMeter: function() {

      if (this.model.state !== Config.STATE.IDLE) return;

      this.model.state = Config.STATE.POWER;
      DoubleClick.sendCounter(DoubleClick.COUNTERS.POWER);

      // change to release message and start power meter:
      this.instructions.setCurrentMessage(Instructions.MESSAGES.RELEASE);
      this.powerMeter.start();
    },

    kick: function() {

      // if ball is active, ignore shoot request:
      if (this.model.state !== Config.STATE.POWER) return;
      this.model.state = Config.STATE.KICK;
      DoubleClick.sendCounter(DoubleClick.COUNTERS.KICK);
      log(this + '::kick()');

      // disable power button:
      utils.hideHandCursor();
      this.powerMeter.setEnabled(false);
      this.instructions.hide();
      this.powerMeter.stop();

      // reset ball properties for kick:
      this.ball.body.allowGravity = true;
      this.ball.checkWorldBounds = true;
      this.ball.revive();

      // play kick animation:
      this.player.kick();

      // set angle of kick:
      var angle = Config.KICK_RANGE.min +
        (Config.KICK_RANGE.max - Config.KICK_RANGE.min) * this.powerMeter.value;

      // FIXED ANGLE FOR TESTING:
      //angle = Phaser.Math.degToRad(-22); // mid target

      // rotate ball during kick:
      this.game.add.tween(this.ball)
        .to({rotation: Phaser.Math.degToRad(-720)},
            1000, Phaser.Easing.Linear.None, true);

      // add velocity vector to ball to set it moving:
      this.ball.body.velocity.set(
          Math.cos(angle) * Config.KICK_SPEED,
          Math.sin(angle) * Config.KICK_SPEED
      );

      // queue up return animation:
      setTimeout(function() {
        this.player.return();
      }.bind(this), 500);
    },

    onBallOutOfBounds: function() {
      //log(this + '::onBallOutOfBounds() - trigger MISS animation');

      this.ball.kill();
      this.onTargetMiss();
    },

    // --------------------------- HIT SEQUENCES ---------------------------- //
    onTargetHit: function(idx) {
      log(this + '::onTargetHit(' + idx + ')');

      this.model.state = Config.STATE.RESULT;
      DoubleClick.sendCounter(DoubleClick.COUNTERS.HIT);

      this.powerMeter.reset();
      this.showTargetHit(idx);
      this.model.incrementScore();
      this.lifeMeter.hit(this.model.attempt);
    },

    showTargetHit: function(targetIdx) {
      // Spin target:
      // ToDo: spin target with decreasing speed until stop
      var target = this.targetGroup.getAt(targetIdx);
      this.game.add.tween(target.scale)
        .to({x: 0}, 100, Phaser.Easing.Circular.InOut, true, 0, 7, true);

      // get hit anim:
      this.hitAnim.visible = true;
      this.hitAnim.position.setTo(
          this.targetGroup.x + target.x,
          this.targetGroup.y + target.y
      );
      this.hitAnim.stars.animations.play('burst');

      var spd = 1000;
      this.game.add.tween(this.hitAnim.label.scale)
        .to({x: 1.5, y: 1.5}, spd, Phaser.Easing.Quadratic.Out, true);

      this.game.add.tween(this.hitAnim.label)
        .to({alpha: 0}, spd + 100, Phaser.Easing.Quadratic.Out, true)
        .onComplete.add(this.onHitAnimComplete, this);
    },

    onHitAnimComplete: function() {
      this.hitAnim.visible = false;
      this.hitAnim.label.alpha = 1;
      this.hitAnim.label.scale.setTo(1, 1);
      this.hitAnim.stars.frame = 'star_1.png';

      setTimeout(this.reset.bind(this), 400);
    },

    // -------------------------- MISS SEQUENCES ---------------------------- //
    onTargetMiss: function() {
      log(this + '::onTargetMiss()');

      this.model.state = Config.STATE.RESULT;
      DoubleClick.sendCounter(DoubleClick.COUNTERS.MISS);

      this.powerMeter.reset();
      this.lifeMeter.miss(this.model.attempt);
      this.showMissSign();
    },

    showMissSign: function() {
      this.sign.visible = true;
      this.game.add.tween(this.sign)
        .to({x: this.sign.showPos.x},
            150, Phaser.Easing.Quadratic.Out, true, 200)
        .to({x: this.sign.showPos.x + 10, y: this.sign.showPos.y + 2},
            100, Phaser.Easing.Quadratic.InOut, true, 0, 10, true);

      // hide sign after 1.5 secs:
      setTimeout(this.hideMissSign.bind(this), 1500);
    },

    hideMissSign: function() {
      // hide sign and trigger reset:
      this.game.add.tween(this.sign)
        .to({x: this.sign.hidePos.x, y: this.sign.hidePos.y},
            400, Phaser.Easing.Quadratic.In, true)
        .onComplete.add(
        function() {
          this.sign.visible = false;
          this.reset();
        }, this);
    },

    showDude: function() {
      log(this + '::showDude()');
      this.dude.visible = true;

      // bring on the big man!
      this.game.add.tween(this.dude)
        .to({x: this.dude.showPos.x, y: this.dude.showPos.y},
            300, Phaser.Easing.Back.Out, true, 200);
    },

    showEndScreen: function() {

      log(this + '::showEndScreen()');

      DoubleClick.sendCounter(DoubleClick.COUNTERS.GAME_OVER);

      // show overlay:
      this.overlay.visible = true;
      this.overlay.alpha = 0;

      this.game.add.tween(this.overlay)
        .to({alpha: 1}, 200, Phaser.Easing.Quadratic.Out, true)
        .onComplete.add(
        function() {
          this.showDude();
          this.endScreen.show(this.model.score);
        }, this
      );
    },

    hideEndScreen: function() {
      log(this + '::hideEndScreen()');

      //this.gameOverButtons.visible = false;
      this.onGameComplete.dispatch();

      // hide overlay and message:
      var spd = 350;
      this.game.add.tween(this.overlay)
        .to({alpha: 0}, spd, Phaser.Easing.Quadratic.Out, true);

      this.game.add.tween(this.endScreen)
        .to({alpha: 0}, spd, Phaser.Easing.Quadratic.Out, true);

      this.game.add.tween(this.dude)
        .to({alpha: 0}, spd, Phaser.Easing.Quadratic.Out, true)
        .onComplete.add(this.reset, this);
    },

    onButtonOver: function(target) {
      utils.showHandCursor();
      target.alpha = 0;
    },
    onButtonOut: function(target) {
      utils.hideHandCursor();
      target.alpha = 1;
    },

    onEndScreenDecision: function(choice) {
      switch(choice) {
        case EndScreen.EVENT.EXIT:
          this.onExitBtnClick();
          break;

        case EndScreen.EVENT.RESET:
          DoubleClick.sendCounter(DoubleClick.COUNTERS.REPLAY);
          this.hideEndScreen();
          break;
      }
    },

    onExitBtnClick: function() {
      DoubleClick.exit(DoubleClick.EXITS.CTA);
    },

    reset: function() {
      log(this + '::reset()');

      this.model.state = Config.STATE.RESET;

      // Check for game end:
      if (this.model.isGameComplete()) {
        this.showEndScreen();
        return;
      }

      // Increment attempt count:
      this.model.incrementAttempt();

      // reset game elements:
      this.dude.position.setTo(this.dude.hidePos.x, this.dude.hidePos.y);
      this.dude.alpha = 1;
      this.dude.visible = false;
      this.endScreen.hide();
      this.overlay.visible = false;

      // reset ball properties:
      this.ball.revive();
      this.ball.reset(this.ball.startPos.x, -20);
      this.ball.scale.setTo(1);
      this.ball.checkWorldBounds = false;
      this.ball.body.allowGravity = false;

      // reset power meter:
      if (this.powerMeter.value > 0.01) this.powerMeter.reset();

      // drop ball from top of screen:
      this.game.add.tween(this.ball)
        .to({y: this.ground.y - this.ball.height * 0.5},
            1000, Phaser.Easing.Bounce.Out, true)
        .onComplete.add(this.onResetComplete, this);

      // ball rotation during drop:
      this.game.add.tween(this.ball)
        .to({rotation: this.ball.rotation + Phaser.Math.degToRad(180)},
            1000, Phaser.Easing.Quadratic.Out, true);
    },

    onResetComplete: function() {
      log(this + '::onResetComplete()');
      this.model.state = Config.STATE.IDLE;

      // enable power button and show hold message:
      this.powerMeter.setEnabled(true);
      this.instructions.setCurrentMessage(Instructions.MESSAGES.HOLD);
      this.instructions.show();
    },

    getTargetHit: function() {
      var target = -1;
      this.targetGroup.forEach(function(item) {
        if(this.game.physics.arcade.collide(this.ball, item)) {
          target = item.parent.getIndex(item);
        }
      }, this);
      return target;
    },

    resetView: function() {
      this.lifeMeter.reset();
    },

    updateBall: function() {
      if (this.model.state !== Config.STATE.KICK) return;

      // Shrink ball as is nears targets:
      var shrinkage = 0.15;
      var s = (this.targetGroup.x - this.ball.x) /
        (this.targetGroup.x - this.ball.startPos.x);
      this.ball.scale.setTo(1 - shrinkage + (shrinkage * s));

      // drop ball trail:
      if (this.ball.x - this.ball.startPos.x > this.ball.width) {
        var trail = this.game.add.graphics(
          this.ball.x, this.ball.y, this.trailPool
        );
        trail.beginFill(0xFFFFFF, 0.5);
        trail.drawCircle(0, 0, this.ball.width * 0.35);
        trail.scale.x = 1.5;
        trail.endFill();

        // animate out alpha and scale:
        this.game.add.tween(trail)
          .to({alpha: 0}, 300, Phaser.Easing.Quadratic.Out, true)
          .onComplete.add(
          function(target) {
            target.parent.remove(target, true);
          });
        this.game.add.tween(trail.scale)
          .to({x: 0, y: 0}, 500, Phaser.Easing.Quadratic.Out, true);
      }
    },

    // game loop:
    update: function() {

      // Test for target collisions:
      var idx = this.getTargetHit();
      if (idx !== -1) {
        this.ball.kill();
        this.onTargetHit(idx);
      }

      // Draw ball trail and manage ball scaling during kick phase:
      this.updateBall();

      // Update player elements:
      if(this.model.state === Config.STATE.POWER) {
        this.player.setPose(this.powerMeter.value);
      }
    },


    toString: function() {
      return 'GameView';
    }
  };

  window.GameView = GameView;

})();