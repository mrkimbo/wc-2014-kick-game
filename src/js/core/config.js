/**
 * Created with WebStorm.
 * User: kimh
 * Date: 18/06/2014
 * Time: 16:13
 */

/* global Phaser */

(function() {
  'use strict';

  var Config = {

    GRAVITY: 980, // pixels/second/second
    KICK_SPEED: 1200,
    POWER_BAR_SPEED: 800,

    ALLOW_RETINA: false,
    USE_RETINA: false,
    FONTS_LOADED: false,

    SCORE: 0,
    MAX_ATTEMPTS: 3,

    KICK_RANGE: {
      min: Phaser.Math.degToRad(-5),
      max: Phaser.Math.degToRad(-38)
    },

    STATE: {
      INTRO: 'GameState:Intro',
      IDLE: 'GameState:Idle',
      POWER: 'GameState:Power',
      KICK: 'GameState:Kick',
      RESULT: 'GameState:Results',
      RESET: 'GameState:Reset'
    }
  };

  window.Config = Config;

})();
