/**
 * Created with WebStorm.
 * User: kimh
 * Date: 18/06/2014
 * Time: 13:27
 */

/* global Enabler, log */

(function() {
  'use strict';

  var DoubleClick = {};

  DoubleClick.COUNTERS = {
    POWER: 0,
    KICK: 1,
    HIT: 2,
    MISS: 3,
    GAME_OVER: 4,
    REPLAY: 5
  };
  DoubleClick.EXITS = {
    CTA: 0
  };

  DoubleClick.sendCounter = function(type) {
    switch (type) {
      case this.COUNTERS.POWER:
        Enabler.counter('PowerMeterStart', true);
        break;

      case this.COUNTERS.KICK:
        Enabler.counter('Kick', true);
        break;

      case this.COUNTERS.HIT:
        Enabler.counter('Hit', true);
        break;

      case this.COUNTERS.MISS:
        Enabler.counter('Miss', true);
        break;

      case this.COUNTERS.GAME_OVER:
        Enabler.counter('GameComplete', true);
        break;

      case this.COUNTERS.REPLAY:
        Enabler.counter('Replay', true);
        break;

      default:
        log('Metrics::sendCounter() - unsupported counter: ' + type);
        break;
    }
  };

  DoubleClick.exit = function(type) {
    switch(type) {
      case DoubleClick.EXITS.CTA:
        Enabler.exit('Exit:CTA');
        break;
    }
  };

  window.DoubleClick = DoubleClick;

})();