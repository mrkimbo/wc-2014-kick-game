/**
 * Created with WebStorm.
 * User: kimh
 * Date: 13/06/2014
 * Time: 15:17
 */

/* global log, Config, Metrics, utils */

(function() {

  'use strict';

  var GameModel = function() {};
  GameModel.prototype = {

    init: function(initialState) {
      this.reset();
      this.createAccessors();

      this._state = initialState;
    },

    incrementAttempt: function() {
      this._attempt++;
    },

    incrementScore: function() {
      this._score ++;
    },

    isGameComplete: function() {
      return this.attempt === Config.MAX_ATTEMPTS;
    },

    reset: function() {
      this._score = 0;
      this._attempt = 0;
    },

    createAccessors: function() {

      Object.defineProperties(
        this, {
          'state': {
            get: function() { return this._state; },
            set: function(v) {
              if(v === this._state) return;
              this._state = v;
            }
          },
          'score': {
            get: function() { return this._score; }
          },
          'attempt': {
            get: function() { return this._attempt; }
          }
        }
      );
    },

    toString: function() {
      return 'GameModel';
    }
  };

  window.GameModel = GameModel;

})();