/**
 * Created with WebStorm.
 * User: kimh
 * Date: 6/06/2014
 * Time: 10:48
 */

function gameDebug() {

  //game.debug.body(this.ball, '#CC0000');
  //game.debug.body(this.ground);
  //game.debug.body(this.targetGroup.getAt(0));
  //game.debug.body(this.targetGroup.getAt(1));
  //game.debug.body(this.targetGroup.getAt(2));

  //game.debug.spriteBounds(this.sign);

  //game.debug.spriteBounds(this.resultMessages);

  //game.debug.spriteBounds(this.view.player, 'rgba(200,0,0,0.3)', true);

  //game.debug.spriteBounds(this.view.lifeMeter);
  //game.debug.spriteBounds(this.view.lifeMeter.bg, '#CC0000', false);
  //game.debug.spriteBounds(this.view.lifeMeter.results, '#0000CC', false);
}

// ------------------------------- SCRATCH PAD ------------------------------ //


// DRAWING GRADIENT:
/*var bmp = game.add.bitmapData(300,30);
 var grd = bmp.context.createLinearGradient(0,0,300,0);
 grd.addColorStop(0,'#CC0000');
 grd.addColorStop(1,'#0000CC');
 bmp.fill(0,0,0,1);
 bmp.context.fillStyle = grd;
 bmp.context.fillRect(2,2,296,26);
 this.powerMeter = game.add.sprite(
 Math.round((game.width-bmp.width) *0.5),
 game.height, bmp
 );*/


// DRAW CURVE TRAJECTORY:
/*GameState.prototype.drawTrajectory = function() {
 // Clear the bitmap
 this.bitmap.context.clearRect(0, 0, this.game.width, this.game.height);

 // Set fill style to white
 this.bitmap.context.fillStyle = 'rgba(255, 255, 255, 0.5)';

 // Calculate a time offset. This offset is used to alter the starting
 // time of the draw loop so that the dots are offset a little bit each
 // frame. It gives the trajectory a "marching ants" style animation.
 var MARCH_SPEED = 40; // Smaller is faster
 this.timeOffset = this.timeOffset + 1 || 0;
 this.timeOffset = this.timeOffset % MARCH_SPEED;

 *//**
 * Just a variable to make the trajectory match the actual track a
 * little better. The mismatch is probably due to rounding or the physics
 * engine making approximations.
 *//*
 var correctionFactor = 0.99;

 // Draw the trajectory
 // http://en.wikipedia.org/wiki/Trajectory_of_a_projectile#Angle_required_to_hit_coordinate_.28x.2Cy.29
 var theta = -this.player.rotation;
 var x = 0, y = 0, t = this.timeOffset / (1000 * MARCH_SPEED / 60);
 for (t; t < 3; t += 0.03) {
 x = this.KICK_SPEED * t * Math.cos(theta) * correctionFactor;
 y = this.KICK_SPEED * t * Math.sin(theta) * correctionFactor -
 0.5 * this.GRAVITY * t * t;
 this.bitmap.context.fillRect(
 x + this.player.group.x, this.player.group.y - y, 3, 3
 );
 if (y < -15) break;
 }

 this.bitmap.dirty = true;
 };*/