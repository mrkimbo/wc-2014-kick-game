/**
 * Created with WebStorm.
 * User: kimh
 * Date: 3/06/2014
 * Time: 16:37
 */

/* global Enabler, Config, GameView, Metrics, GameModel, studio, Phaser, log */

// Dependencies: Phaser 2.0.4 framework

// Copyright © 2014 John Watson
// Licensed under the terms of the MIT License

(function() {

  'use strict';

  window.WebFontConfig = {

    active: function() {
      //log(this);
    },

    google: {
      families: ['Open Sans']
    }
  };


  var PreloadState = function() {};
  PreloadState.prototype = {

    preload: function() {
      game.load.script(
        'webfont',
        '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js',
        this.onFontsLoaded,
        this
      );
    },

    onFontsLoaded: function() {
      game.time.events.add(Phaser.Timer.SECOND, function() {
        log(this + '::onFontsLoaded()');
        Config.FONTS_LOADED = true;
        this.create();
      }, this);
    },

    create: function() {
      if(!Config.FONTS_LOADED) return;

      this.initStage();

      // trigger intro scene:
      game.state.start('game');
    },

    initStage: function() {
      var context = game.canvas.getContext('2d'),
        ratio = window.devicePixelRatio;

      log(this + '::initStage() - PixelDensity: ' + ratio);

      // add smoothing:
      game.stage.smoothed = true;

      // detect retina:
      if (Config.ALLOW_RETINA && context && ratio === 2) {
        log(this + '::initStage() - scale for retina');
        var size = new Phaser.Point(game.canvas.width, game.canvas.height);
        game.canvas.width *= ratio;
        game.canvas.height *= ratio;
        context.scale(ratio, ratio);
        game.canvas.style.width = size.x + 'px';
        game.canvas.style.height = size.y + 'px';

        Config.USE_RETINA = true;
      }
    },

    toString: function() {
      return 'Preload';
    }

  };

  var GameState = function() {

    this.model = new GameModel();
    this.model.init(Config.STATE.INTRO);

  };
  // -------------------------- GAME BUILD SECTION -------------------------- //
  GameState.prototype = {

    preload: function() {
      log(this + '::preload()');

      game.load.atlasJSONHash(
        'sprites',
        this.processFilename('img/sprites.png'),
        this.processFilename('img/sprites.json')
      );
    },

    processFilename: function(filename) {
      var suffix = Config.USE_RETINA ? '@2x' : '';
      return filename.replace(/(\.\w+)$/, suffix + '$1');
    },

    create: function() {

      this.view = new GameView();
      this.view.init(this.game, this.model);
      this.view.onGameComplete.add(this.onGameComplete, this);

      // Show FPS
      this.game.time.advancedTiming = true;
      this.fpsText = this.game.add.text(
        20, 20, '', { font: '16px Arial', fill: '#ffffff' }
      );

      // Kill loader:
      var loader = document.getElementById('loader');
      loader.parentNode.removeChild(loader);
      loader = null;

      // enable gravity:
      game.physics.arcade.gravity.y = Config.GRAVITY;

      this.resetGame();
      this.view.playIntro();
    },

    onGameComplete: function() {
      log(this + '::onGameComplete()');
      this.resetGame();
    },

    resetGame: function() {
      this.model.reset();
      this.view.resetView();
    },

    toString: function() {
      return 'GameState';
    }

  };

  // The update() method is called every frame
  GameState.prototype.update = function() {
    this.view.update();
  };


  // DEBUG!
  GameState.prototype.render = window.gameDebug;

  // Entry Point:
  var game = window.game = new Phaser.Game(900, 250, Phaser.CANVAS, 'kicker');
  game.transparent = true;
  game.state.add('preload', PreloadState, false);
  game.state.add('game', GameState, false);


  if (!window.Enabler) {
    init();
  } else {
    if(Enabler.isInitialized()) {
      init();
    } else {
      Enabler.addEventListener(studio.events.StudioEvent.INIT, init);
    }
  }

  function init() {
    log('init()');
    game.state.start('preload');
  }

})();
